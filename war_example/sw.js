/*
 *
 *  Air Horner
 *  Copyright 2015 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */

const version = "0.6.8";
const cacheName = 'hittaordet-${version}';
self.addEventListener('install', e => {
  const timeStamp = Date.now();
  e.waitUntil(
    caches.open(cacheName).then(cache => {
      return cache.addAll([
        '/HittaOrdet/',
		'/HittaOrdet/#',
        '/HittaOrdet/index.html',
        '/HittaOrdet/css/jasny-bootstrap.min.css',
        '/HittaOrdet/css/navmenu-reveal.css',
        '/HittaOrdet/css/bootstrap-switch.min.css',
        '/HittaOrdet/css/bootstrap.min.css',
		'/HittaOrdet/js/jquery-3.1.0.min.js',
		'/HittaOrdet/js/engine.js',
		'/HittaOrdet/js/jasny-bootstrap.min.js',
		'/HittaOrdet/js/swipe.js',
		'/HittaOrdet/js/bootstrap.min.js',
		'/HittaOrdet/js/bootstrap-switch.min.js',
		'/HittaOrdet/img/ajax-loader.gif',
		'/HittaOrdet/fonts/glyphicons-halflings-regular.woff2',
		'/HittaOrdet/css/bootstrap.min.css',
		'/HittaOrdet/css/bootstrap.min.css',
		'/HittaOrdet/sw.js',
        '/favicon.ico'
      ])
          .then(() => self.skipWaiting());
    })
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.open(cacheName)
      .then(cache => cache.match(event.request, {ignoreSearch: true}))
      .then(response => {
      return response || fetch(event.request);
    })
  );
});